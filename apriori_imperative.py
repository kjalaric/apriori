'''
apriori_imperative.py

Implementation of apriori algorithm in python. Not class-based
kjalaric@gitlab

'''

from itertools import combinations

items_base = {'1': 'alpha',
         '2': 'beta',
         '3': 'theta',
         '4': 'delta',
         '5': 'epsilon',
         '6': 'zeta'}

transactions = ['1 2 3 4',
                '1 2 3 5',
                '1 2 4 5',
                '1 2 5 6',
                '3 4 5 6',
                '1 2 5 6']

def getItemFrequency(transaction_list, items):
    item_frequency = {key: 0 for key in items}
    for transaction in transactions:
        for item in items:
            if (item in transaction):
                item_frequency[item] += 1
    return item_frequency

def reduceItemSet(items, item_frequencies, support_threshold):
    reduced_items = list(items)
    pruned_items = list()
    for item in reduced_items:
        if item_frequencies[item] < support_threshold:
            reduced_items.remove(item)
            pruned_items.append(item)
    return reduced_items, pruned_items

def generatePairs(reduced_items, combination_size):
    # assuming item size is the same for all items in the list
    coms = list(combinations(reduced_items, combination_size))
    pairs = list()
    for com in coms:
        com_joined = ''
        for item in com:
            com_joined += item
            com_joined += ' '
        com_joined = com_joined[:-1]
        pairs.append(com_joined)
    return pairs


if __name__ == "__main__":
    
    items = items_base.keys()
    print ("items:", items)
    item_freq = dict()  # for old_item_frequencies
    
    iterations = 0
    while(1):
        iterations += 1
        print ("\nIterations: ", iterations)
        old_item_frequencies = item_freq
        
        item_freq = getItemFrequency(transactions, items)
        print ("item_freq:", item_freq)
        
        reduced_items, pruned_items = reduceItemSet(items, item_freq, 4)
        print ("reduced_items:", reduced_items)
        print ("pruned_items:", pruned_items)
        
        try:
            items = generatePairs(reduced_items, iterations+1)
            print ("items:", items)
        except IndexError:
            print ("Operation complete - IndexError in generatePairs()")
            break
        
        if len(items) == 0:
            print("Operation complete - cannot generate smaller combinations")
            print("best frequencies: ", old_item_frequencies)
            break
            
    

    
    
    